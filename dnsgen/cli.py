import click
from . import dnsgen


@click.command()
@click.option('-l',
              '--wordlen',
              default=4,
              help='Min length of custom words extracted from domains.',
              required=False,
              type=click.IntRange(1, 100))
@click.option('-w',
              '--wordlist',
              default=None,
              help='Path to custom wordlist.',
              type=click.Path(exists=True, readable=True),
              required=False)
@click.option('-f',
              '--fast',
              default=None,
              help='Fast generation.',
              is_flag=True,
              required=False)
@click.option('-l',
              '--limit',
              default=1000000000,
              help='Maximum number of output lines.',
              type=click.IntRange(1),
              required=False)
@click.argument('filename', required=True, type=click.File(mode='r'))
def main(wordlen, wordlist, filename, fast, limit):
    # read the input
    domains = filename.read().splitlines()

    results = set()
    for r in dnsgen.generate(domains, wordlist, wordlen, fast=fast):
        if results is not None and len(results) >= limit:
            break

        if r not in results:
            results.add(r)
            click.echo(r)
